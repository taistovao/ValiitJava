package it.vali;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    String firstWord;
    String secondWord;

    public static void main(String[] args) {
        // 1.Arvuta ringi pindala, kui teada on raadius.
        // Prindi pindala välja ekraanile
        // S = Pi * R2
        int radius = 5;
        double pi = 3.14;
        double ringiPindala = (pi * (radius * radius));
        System.out.println(ringiPindala);

        int[] numbers = new int[]{3, 6, 7};
        String[] words = intArraytoStringArray(numbers);
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }


        // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse
        // ja mille sisendparameetriteks on kaks Stringi.
        // Meetod tagastab kas tõene või vale selle kohta, kas
        // Stringid on võrdsed.
        System.out.println(newMethod("Kala", "Kala"));
    }

    public static boolean newMethod(String firstWord, String secondWord) {

        if (firstWord.equals(secondWord)) {
            return true;
        }
        return false;
    }


// 3. Kirjuta meetod, mille sisendparameetriks on
    // täisarvude massiiv ja mis tagastab Stringide massiivi.
    // Iga sisend massiivi elemendi kohta olgu tagastatavas massiivis samapalju a-tähti
    // 3,6,7 "aaa" "aaaaaa" "aaaaaaa"

    static String[] intArraytoStringArray(int[] numbers) {
        String[] words = new String[numbers.length];

        for (int i = 0; i < words.length; i++) {
            words[i] = generateAString(numbers[i]);
        }
        return words;
    }
    static String generateAString(int count) {
        String word = "";
        for (int i = 0; i < count; i++) {
            word += "a";
        }
        return word;


    }

    // 4. Kirjuta meetod, mis võtad sisendparameetrina aastaarvu
    // sisestada saab ainult aastaid vahemikus 500-2019
    // ja tagastab kõik sellel sajandil esinenud liigaastad.
    // Ütle veateada, kui aastaarv ei mahu vahemikku



    static List<Integer> leapYearsInCentury (int year) {
        List<Integer> years = new ArrayList<Integer>();
        if (year<500 || year > 2019){
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return years;
        }
        int centuryStart = year/100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        for (int i = 2020; i >= centuryStart  ; i-=4 ) {
            if (centuryEnd >= i &&i % 4 == 0) {
            years.add(i);
            }
        }
        return years;
    }

    // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list
    // riikide nimedega, kus seda keelt räägitakse (valin ise keele).
    // Kirjuta üle selle klassi meetod toString() nii, et see
    // tagastab riikide nimekirja eraldades komaga.
    // Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega
    // ning prindi välja selle objekti toString() meetodi sisu.



}