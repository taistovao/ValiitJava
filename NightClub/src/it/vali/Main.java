package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        String firstName = "Lauri";
        String lastName = "Mattus";

        System.out.println("Tere, kuidas on sinu nimi?");
        Scanner scanner = new Scanner(System.in);
        String külastaja = scanner.nextLine();

        if(firstName.toLowerCase().equals(külastaja.toLowerCase())) {
            System.out.printf("Tere %s%n", külastaja);

            System.out.println("Kui vana sa oled?");
            int age = Integer.parseInt(scanner.nextLine());
            if(age >= 18) {
                System.out.println("Tere tulemast klubisse?");
            } else {
                System.out.println("Sorry, oled alaealine.");
            }
        }
        else {
            System.out.println("Mis on sinu perekonnanimi?");
            String külastaja2 = scanner.nextLine();
            if(külastaja2.equals(lastName.toLowerCase())) {
                System.out.printf("Tere %s sugulane%n", lastName);
            }
            else {
                System.out.println("Sorry, ma ei tunne sind.");
            }

        }

                //kõigepealt küsitakse külastaja nime
                //kui nimi on listis, siis öeldakse kasutajale
                //"Tere tulemast nimi"
                //ja küsitakse külastaja vanust
                //Kui kasutaja on alaealine, teavitatakse teda, et sorry
                //sisse ei pääse, muuljuhl siis öeldakse "tere tulemast klubisse".

                //Kui kasutaja ei olnud listis
                //küsitakse kasutajalt ka tema perekonnanimi
                //kui perekonnanimi on listis
                //siis öeldakse "tere tulemast nimi"
                //muul juhul öeldakse "ma ei tunne sind".


            }
        }


