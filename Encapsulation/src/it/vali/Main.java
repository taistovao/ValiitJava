package it.vali;

public class Main {

    public static void main(String[] args) {

        // kui väärtus on null , siis ei saa seda kasutada võrdluses.
        //int vaikeväärtus 0
        //boolean vaikeväärtus false
        //double vaikeväärtus 0.0
        //String vaikeväärtus null
        //Objektide (monitor, filewriter) vaikeväärtus null
        //int [] arvud vaikeväärtus null

    Monitor firstMonitor = new Monitor();
    firstMonitor.setDiagonal(-1000);
    System.out.println(firstMonitor.getDiagonal());

    firstMonitor.setManufacturer("Huawei");
    firstMonitor.printInfo();


    }
}
