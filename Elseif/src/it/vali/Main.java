package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        System.out.println("Sisesta arv: ");
        Scanner kysimus = new Scanner(System.in);
        int number = Integer.parseInt(kysimus.nextLine());

        if (number > 3) {
            System.out.println("Number on suurem kui 3");
        }
        else if (number == 0){
            System.out.println("Numbrid on 0");
        }
        else if (number < 3 && number !=0) {
            System.out.println("Number on väiksem kui 3");
        }
        else {
            System.out.println("Numbrid on võrdsed");
        }
        }
    }

