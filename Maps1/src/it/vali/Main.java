package it.vali;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        // Map järjekorda ei salvesta.
        Map<String, String> map = new HashMap<String, String>();
             // Kui tahame et säiliks lisamise järjekord, kasutame LinkedHashMapi

        // Sõnaraamat
        // key => value
        // Maja => House
        // Isa => Dad
        // Puu => Tree

        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");

        // Oletame, et tahan teada, mis on inglise keeles Puu

        String translation = map.get("Puu");
        System.out.println(translation);

        Map<String, String> idNumberName = new HashMap<String, String>();
        idNumberName.put("23423424244", "Lauri");
        idNumberName.put("43545345345", "Peeter");
        //Kui kasutada put sama key lisamisel, kirjutatakse value üle

        System.out.println(idNumberName.get("23423424244"));

        // Loe lauses üle kõik erinevad tähed
        // ning prindi välja iga tähe järel,
        // mitu tükki teda selles lauses oli

        //Char on selline tüüp, kus saab hoida üksikut sümbolit.
        char symbolA = 'a';
        char symbolB = 'b';
        char newLine = '\n';

        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            if (letterCounts.containsKey(characters[i])) {
            letterCounts.put(characters[i],letterCounts.get(characters[i] + 1));
            } else {
            letterCounts.put(characters[i], 1);
            }
        }
        // Map.Entry<Character, Integer> on klass, mis hoiab endas ühte rida map-is.
        // ehk ühte key-value paari

        System.out.println(letterCounts);
        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) {
            System.out.printf("Tähte %s esines %d korda", entry.getKey(),entry.getValue());
        }
    }
}

    // e2
    // l1
    // a2
    // s3


