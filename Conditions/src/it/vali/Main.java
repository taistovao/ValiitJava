package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta number: ");
        Scanner lisa = new Scanner(System.in);
        int a = Integer.parseInt(lisa.nextLine());
        //java on type-safe language
        if (a == 3) {
            System.out.printf("sama %n");
        }
        if (a < 5) {
            System.out.printf("number %d is smaller than five%n", a);
        }
        if (a > 5) {
            System.out.printf("number %d is bigger than three%n", a);
        }
        if (a != 4) {
            System.out.printf("arv %d ei võrdu neljaga%n", a);
        }
        if (a >= 7) {
            System.out.printf("Arv %d on suurem või võrdne seitsmest%n", a);
        }
        if (!(a >= 7)) {
            System.out.printf("Arv %d ei ole suurem või võrdne seitsmega%n", a);
        }
        if (a > 2 && a < 8) {
            System.out.printf("Arv %d on suurem kui kaks ja väiksem kui kaheksa", a);
        }
        if (a < 8 || a > 8) {
            System.out.printf("Arv %d on väiksem kui 2 või arv %d on suurem kui 8%n", a);
        }
        if (a > 2 && a < 8 || a > 5 && a < 8 || a > 10) {
            System.out.printf("gjghh %d %N", a);
        }
        if ((!(a > 4 && a < 6) && (a > 5 && a < 8)) || (a<0 && !(a > 14))) {
            System.out.printf("wwwwwwww");
        }
            // kui üks tingimus on vale, siis IF funktsioon edasi ei kontrolli ja lõpetab.
            // kui tingimuste vahel on üks või õige, siis on väide tõene, kui tingimuste vahel on üks JA vale,
            // siis on tingimus false.
        



        }

    }
