package it.vali;
import java.util.Scanner;
public class Main {
    //küsi kasutajalt 2 arvu, seejärel küsi kasutajalt, mis tehet ta soovib teha
    // a)liitmine, b)lahutamine, c)korrutamine, d)jagamine
    // prindi kasutajale tehte vastus
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        do {
            do {
                System.out.println("Sisesta esimene number");
                int a = Integer.parseInt(scanner.nextLine());

                System.out.println("Sisesta teine number");
                int b = Integer.parseInt(scanner.nextLine());

                System.out.println("Kas soovid arvud a-liita, b-lahutada, c-korrutada, d-jagada?");
                String matemaatika = (scanner.nextLine());

                if (matemaatika.equals("a")) {
                    System.out.println(sum(a, b));
                } else if (matemaatika.equals("b")) {
                    System.out.println(subtract(a, b));
                } else if (matemaatika.equals("c")) {
                    System.out.println(multiply(a, b));
                } else if (matemaatika.equals("d")) {
                    System.out.println(divide(a, b));
                } else
                    System.out.println("Selline tehe puudub");

                System.out.println("Kas tahad veel arvutada? j/e");


            } while (scanner.nextLine().equals("j"));
        } while (scanner.nextLine().equals("j"));


    }


    // Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    // subract
    static int subtract(int a, int b) {
        return a - b;
    }

    // multiply
    static int multiply(int a, int b) {
        return a * b;
    }

    // divide
    static double divide(int a, int b) {
        return (double) a / b;
    }
}

