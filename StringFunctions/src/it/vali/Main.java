package it.vali;

public class Main {

    public static void main(String[] args) {

        String sentence = "kevadel elutses metsas Mutionu keset kuuski noori vanu";

        // Sümbolite indeksid tekstis algavad samamoodi indeksiga 0
        // nagu massiivides
        // Leia üles esimene tühik, mis on tema indeks (asukoht).

        int spaceIndex = sentence.indexOf(" ");
        // indexOf tagastab -1, kui otsitavat fraasi (tähte või sõna) ei leitud
        // ning indeksi (kust sõna algab) kui fraas leitakse.

        while(spaceIndex != -1)
        {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        }
        spaceIndex = sentence.lastIndexOf(" ");
        while (spaceIndex != -1) {
        spaceIndex = sentence.lastIndexOf(" ", spaceIndex -1);
        }
        spaceIndex = sentence.indexOf(" ");

        // Prindi lause esimesed neli tähte.
        String part = sentence.substring(0,spaceIndex);
        System.out.println(part);

        // prindi lause teine sõna
        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex+1);
        String secondWord = sentence.substring(spaceIndex + 1, secondSpaceIndex);
        System.out.println(secondWord);


        //Leia esimene k tähega algav sõna

        String firstLetter = sentence.substring(0,1);
        int KIndex = sentence.indexOf(" k") + 1;
        String kWord = "";

        if (firstLetter.equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0, spaceIndex);


        } else{
            int kIndex = sentence.indexOf(" k") + 1;
            spaceIndex = sentence.indexOf(" ", kIndex);
            kWord = sentence.substring(kIndex, spaceIndex);
            System.out.println(kWord); // printis Keset
        }

        System.out.println("");
        // Leia mitu sõna mul lauses on
        int spaceCounter =1;
        while (spaceIndex != -1) {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }

        System.out.printf("Sõnade arv lauses on %d%n", spaceCounter);

        //Kodune ülesanne : Leia mitu k-tähega algavat sõna on lauses.

        }


    }

