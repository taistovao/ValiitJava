package it.vali;

public class Main {

    public static void main(String[] args) {

        Monitor firstMonitor = new Monitor();
        Monitor secondMonitor = new Monitor();
        Monitor thirdMonitor = new Monitor();

        firstMonitor.manufacturer = "Philips";
        firstMonitor.color = Color.WHITE;
        firstMonitor.diagonal = 27;
        firstMonitor.screenType = ScreenType.AMOLED;

        secondMonitor.manufacturer = "LG";
        secondMonitor.color = Color.BLACK;
        secondMonitor.diagonal = 24;
        secondMonitor.screenType = ScreenType.LCD;

        thirdMonitor.manufacturer = "Sony";
        thirdMonitor.color = Color.GREY;
        thirdMonitor.diagonal = 21;
        thirdMonitor.screenType = ScreenType.AMOLED;

        System.out.println(firstMonitor.manufacturer);
        System.out.println(secondMonitor.screenType);

        firstMonitor.color = Color.BLACK;

        Monitor[] monitors = new Monitor[4];
        monitors[0] = firstMonitor;
        monitors[1] = secondMonitor;
        monitors[2] = thirdMonitor;

        monitors[3] = new Monitor();
        monitors[3].manufacturer = "Sony";
        monitors[3].color = Color.GREY;
        monitors[3].diagonal = 21;
        monitors[3].screenType = ScreenType.LCD;

        // lisa massiivi 3 monitori
        // Prindi välja kõigi monitoride tootja, mille diagonaal on suurem kui 25 tolli.

        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > 25) {
                System.out.println(monitors[i].manufacturer);
                System.out.println(" ");
            }


        }

        //Leia monitori värv kõige suuremal monitoril.

        Monitor maxSizeMonitor = monitors[0];
        double maxDiagonal = monitors[0].diagonal;
        System.out.printf("Monitori diagonaal cm on %.2f%n", maxSizeMonitor.diagonalToCm());

        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > maxDiagonal) {
                maxSizeMonitor = monitors[i];
            }
        }
         maxSizeMonitor.printInfo();
        firstMonitor.printInfo();

        System.out.println(maxSizeMonitor.color);
        System.out.println(maxSizeMonitor.manufacturer);
        System.out.println(maxSizeMonitor.screenType);


    }
}
