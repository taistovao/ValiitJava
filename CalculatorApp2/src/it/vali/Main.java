package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisestada kaks arvu. Sisesta esimene arv");

        while (scanner.hasNextLine()) {

            int firstNumber = Integer.parseInt(scanner.nextLine());
            System.out.println("Sisesta teine arv");
            int secondNumber = Integer.parseInt(scanner.nextLine());

            System.out.println("Mis tehet soovite teha? \n a) liitmine \n b) lahutamine \n c) korrutamine \n d)jagamine");
            String whatTodo = scanner.nextLine().toLowerCase();

            switch (whatTodo) {
                case "a":
                    System.out.println("Arvude summa on:" + sumcalculator(firstNumber, secondNumber));
                    break;
                case "b":
                    System.out.println("Arvude vahe on: " + subtract(firstNumber, secondNumber));
                    break;
                case "c":
                    System.out.println("Arvude korrutis on: " + multiply(firstNumber, secondNumber));
                    break;
                case "d":
                    System.out.println("Arvude jagatis on: " + divide(firstNumber, secondNumber));
                    break;

                default:
                    System.out.println("Error Pead sisestama tähed a-d ");
            }
            System.out.println("Sisestada esimene arv");
        }
    }
    // Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int sumcalculator(int a, int b) {
        int sum = a + b;
        return sum;
    }

    // subract
    static int subtract(int a, int b) {
        return a - b;
    }

    // multiply
    static int multiply(int a, int b) {
        return a * b;
    }

    // divide
    static double divide(int a, int b) {
        return (double)a / b;
    }
}
