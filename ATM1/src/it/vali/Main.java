package it.vali;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Scanner;

public class Main {

    static String pin;

    public static void main(String[] args) {
        //Lisada konto väljavõtte funktsionaalsus
        //Konto väljavõte:
        // Sularaha sissemakse +100
        // Sularaha väljamakse -100
        // Kontojääk:
        //Step into (debug) läheb
        // Hoiame pin koodi failis pin.txt
        // ja kontojääki balance.txt

        int balance = loadBalance();
        String pin = loadPin();

        int enterpin = 0;
        // pin muutmine
        pin = "3345";
        savePin(pin);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisesta kaart");
        String paus = scanner.nextLine();

        System.out.println(validatePin());



        // raha välja võtmine
        int amount = 100;
        balance -= 100;
        saveBalance(balance);
    }
    static void savePin(String pin) {
        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }
    }
    static String loadPin() {
        // Toimub pin välja lugemine pin.txt failist
        // Toimub shadowing

        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi laadimine ebaõnnestus");
        }
        return pin;
    }
    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt",true); //teeme uue fileWriteri
            fileWriter.write(balance + System.lineSeparator());

            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }

    }
    static int loadBalance() {
        // Toimub balance välja lugemine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt"); //balance loetakse balance.txt välja
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine()); //balance tehakse int
            bufferedReader.close(); //suletakse bufferedreader?
            fileReader.close();//suletakse filereader

        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }
//    static String currentDateTimeToString() {
//
//        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
//        Date date = new Date();
//        return dateFormat.format(date);

        //Move calendar to yesterday
        //Kodune ülesanne ; konto väljavõte

//    }
//    static boolean validatePin() {
//        Scanner scanner = new Scanner(System.in);
//
//        for (int i = 0; i < 3; i++) {
//            System.out.println("Palun sisesta PIN kood");
//            String enteredPin = scanner.nextLine();
//
//            if(enteredPin.equals(loadPin())) {
//                System.out.println("Tore! Õige pin kood");
//                return false;
//            }
//        }
//        return true;
    }




