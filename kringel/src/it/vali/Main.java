package it.vali;

public class Main {

    public static void main(String[] args) {

        String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";

        String[] words = sentence.split(" ja | |, ");

        for (int i = 0; i < words.length ; i++) {
            System.out.println(words[i]);
        }

        String newSentence = String.join(" ", words);

        System.out.println(newSentence);


    }
}
