package it.vali;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {

        // Lõpmatu tsükkel kasutades while
        // while (true) {
        // System.out.println("True");
        // }


                            //Programm mõtleb numbri
                            // programm küsib kasutajalt "arva number ära".
                            // senikaua kuni kasutaja arvab valesti, ütleb "proovi uuesti".
        Random juhuslik = new Random();
                            //random.nextInt (5) genereerib numbri 0 kuni 4
        int number3 = ThreadLocalRandom.current().nextInt(1, 6);
                            //int number3 = +juhuslik.nextInt(5)+1;
                            //80-100
                            //int number3 = random.nextInt(20) + 80;

        System.out.println("Arva number ära: ");
        Scanner scanner = new Scanner(System.in);
        int arv = Integer.parseInt(scanner.nextLine());
                            //while tsükkel on tsükkel kus korduste arv ei ole teada
        while (arv != number3) {
            System.out.println("proovi uuesti!");
            arv = Integer.parseInt(scanner.nextLine());
        }
        System.out.println("Tubli! Arvasid numbri ära!");
        System.out.println("kas soovid veel mängida?");
        while (!scanner.nextLine().toLowerCase().equals("ei")) ;
        {
        }
    }
}

















