package it.vali;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        //Try plokis otsitakse /oodatakse Exceptioneid (Erandeid, Viga)
        try {// FileWriter on selline klass, mis tegeleb faili kirjutamisega.
            // sellest klassist objekti loomisel antakse ette faili asukoht
            //faili asukoht võib olla ainult faili nimega kirjeldatud: output.txt
            // sel juhul kirjutatakse faili, mis asub samas kaustas kus meie Main.class.
            // või täispika asukohaga e:\\output.txt
            // try - kogu kood try ploki sees, kui seal kuskil tekib ioexception, siis tee midagi, mis on selles
            // e.printStackTrace
            FileWriter fileWriter = new FileWriter("c:\\users\\opilane\\intelliJ\\output.txt");
            fileWriter.write("Elas metsas Mutionu\r\n");
            fileWriter.write("koerad\r\n");
            fileWriter.write("32434234 + 234234");
            fileWriter.write("klubi");
           fileWriter.close();

            // Catch plokis püütakse kinni kindlat tüüpi exception või kõik exceptionid, mis pärinevad
            // antud exceptionist.
            //
        } catch (IOException e) {
            // printStackTrace tähendab, et prinditakse välja meetodite välja kutsumise
            // hierarhia/ajalugu
            //e.printStackTrace();
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }


    }
}
