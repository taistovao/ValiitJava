package snippet;

public class Snippet {
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
	
	<h1>Auction list</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>Id</th><th>Name</th><th>Object</th><th>Description</th><th>Price</th><th>Time</th><th>Delete</th></tr>
	<c:forEach var="emp" items="${list}"> 
	<tr>
	<td>${emp.id}</td>
	<td>${emp.name}</td>
	<td>${emp.object}</td>
	<td>${emp.description}</td>
	<td>${emp.price}</td>
	<td>${emp.time}</td>
	<td><a href="deleteemp/${emp.id}">Delete</a></td>
	</tr>
	</c:forEach>
	</table>
	<br/>
	
}

