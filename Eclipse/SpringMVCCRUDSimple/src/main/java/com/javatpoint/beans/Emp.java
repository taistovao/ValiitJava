package com.javatpoint.beans;  
public class Emp {  
private int id;  
private String name;  
private String object;  
private String description;  
private int price;
private int time;
private String userName;
private String password;
  
public int getId() {  
    return id;  
}  
public void setId(int id) {  
    this.id = id;  
}  
public String getName() {  
    return name;  
}  
public void setName(String name) {  
    this.name = name;  
}  
public String getObject() {  
    return object;  
}  
public void setObject(String object) {  
    this.object = object;  
}  
public String getDescription() {  
    return description;  
}  
public void setDescription(String description) {  
    this.description = description;  
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}  
public int getTime() {
	return time;
}
public void setTime(int time) {
	this.time = time;

}  
public String getUserName() {
	return userName;
}
	//lisatud
public void setUserName(String userName) {
	this.userName = userName;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}
}