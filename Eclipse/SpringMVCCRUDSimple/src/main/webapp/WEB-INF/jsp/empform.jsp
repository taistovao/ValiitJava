<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add new object to auction list</h1>
       <form:form method="post" action="save">  
      	<table >  
         <tr>  
          <td>Full Name : </td> 
          <td><form:input path="name"  /></td>
         </tr>  
         <tr>  
          <td>Object :</td>  
          <td><form:input path="object" /></td>
         </tr> 
         <tr>  
          <td>Description :</td>  
          <td><form:input path="description" /></td>
         </tr> 
          <tr>
          <td>Price :</td>  
          <td><form:input path="price" /></td>
         </tr> 
         <tr>
          <td>Time :</td>  
          <td><form:input path="time" /></td>
         </tr> 
         <tr>  
          <td> </td>  
          <td><input type="submit" value="Save" /></td>  
         </tr>  
        </table>  
       </form:form>  
