


package it.vali;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner tekst = new Scanner(System.in);
//Esimene tekst
        System.out.println("Mis on sinu nimi: ");
        String name = tekst.nextLine();

        System.out.println("Mis on su lemmik värv?");
        String color = tekst.nextLine();

        System.out.println("Mis telefon sul on ?");
        String telefon = tekst.nextLine();

//Teine tekst
        System.out.println(
                  "Sinu nimi on " + name +". Sinu lemmik värv on " + color +". Sul on " +telefon+ " telefon.");

//Kolmas tekst
        System.out.printf("Sinu nimi on %s. Sinu lemmik värv on %s. Sul on %s telefon.\n",
                name,color, telefon);

//Neljas tekst
        StringBuilder ehitaja = new StringBuilder();
        ehitaja.append("Sinu nimi on ");
        ehitaja.append(name);
        ehitaja.append(".");
        ehitaja.append(" Sinu lemmik värv on ");
        ehitaja.append(color);
        ehitaja.append(".");
        ehitaja.append(" Sul on ");
        ehitaja.append(telefon);
        ehitaja.append(" telefon.");
        System.out.println(ehitaja.toString());

        String fullText = ehitaja.toString();
        System.out.println(fullText);

        String jutuke = String.format("Sinu nimi on %s. Sinu lemmik värv on %s. Sul on %s telefon.\n",
        name,color, telefon);
// Nii System.out.printf kui ka String.format kasutavad enda siseselt StringBuilderit.

        System.out.println(jutuke);
    }
}
