package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Forest implements LivingPlace,LivingInForest  {
    // Loome loomade listi.
    private List<forestAnimal> animals = new ArrayList<forestAnimal>();

    // Siin hoitakse infot, mis loomad metsas võivad olla.
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, kes keda sööb.
    private Map<String, String> whoEatsWho = new HashMap<String, String>();

    // Siin hoitakse infot, palju meil igat looma metsas olla võib.
    private Map<String, Integer> whoMightBe = new HashMap<String, Integer>();

    //Siin lisame metsa loomad(mapping, key - value, listi whoMightBe.)
    public Forest() {
        whoMightBe.put("Wolf", 10);
        whoMightBe.put("Fox", 4);
        whoMightBe.put("Rabbit", 13);
        whoMightBe.put("Mice",30);
        whoMightBe.put("Lynx",25);
        whoMightBe.put("Squirrel", 300);
    }

//    public Forest(String kes, String keda) {
//
//        whoEatsWho.put("Wolf", "Rabbit");
//        whoEatsWho.put("Fox", "Squirrel");
//        whoEatsWho.put("Lynx", "Mice");
//        whoEatsWho.put("Rabbit", "Plants");
//        whoEatsWho.put("Mice", "Insects");
//        whoEatsWho.put("Squirrel", "Nuts");
//
//
//    }
    @Override
    public void addAnimal(Animal animal) {

        // Kas animal on tüübist forestAnimal või pärineb sellest tüübist
        if (forestAnimal.class.isInstance(animal)){
            System.out.println("See loom läks metsa elama");
            return;
        }
        // Vaatame, kas selline loom sobib metsa
        String animalType = animal.getClass().getSimpleName();
        if (!whoMightBe.containsKey(animalType))
        {
            System.out.println("Metsas üldse sellistele loomadele kohta pole");
            return;
        }
        //Teame palju sigu mahub
        int maxAnimalCount = whoMightBe.get(animalType);
        int animalCount = 0;
        if (animalCounts.containsKey(animalType)){
            animalCount = animalCounts.get(animalType);
            if (animalCount >= maxAnimalCount) {
                System.out.println("Metsas on liiga palju selliseid loomi");
                return;
            }
            animals.add((forestAnimal) animal);
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole metsas
            // kindlasti sellele loomale kohta on
        } else {
            animalCounts.put(animalType, 1);
        }
        animals.add((forestAnimal)animal);
        System.out.printf("Metsa lisati loom %s%n", animalType);
    }

    //Tee meetod, mis prindib välja kõik metsas elavad loomad, ning mitu neid on.
    @Override
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    @Override
    public void removeAnimal(String animalType) {

        boolean animalFound = false;
        for (forestAnimal animal : animals) {
            if (animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Metsast lasti see loom maha %s%n", animalType);
                // kui see oli viimane loom, siis eemalda see rida animalCounts map-ist.
                // muuljuhul vähenda animalCounts map-is seda kogust.
                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }
        }
        if (!animalFound) {
            System.out.println("Metsas antud looma pole");

        }
    }

    @Override
    public void animalFoodChain(String whoEatsWho) {
        if (whoEatsWho.equals("Wolf")) {
            System.out.println("Rabbit");
        }  else if (whoEatsWho.equals("Fox")) {
            System.out.println("Squirrel");
        } else if (whoEatsWho.equals("Lynx")) {
            System.out.println("Mice");
        } else if (whoEatsWho.equals("Rabbit")) {
            System.out.println("Plants");
        } else if (whoEatsWho.equals("Mice")) {
            System.out.println("Insects");
        } else if (whoEatsWho.equals("Squirrel")) {
            System.out.println("Nuts");
        } else {
            System.out.println("Selline loom puudub metsast");
        }

    }
}
