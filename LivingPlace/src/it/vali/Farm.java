package it.vali;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

    public class Farm implements LivingPlace {
    //List on interface ja ArrayList on objekt

    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();
    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();
    // Siin hoitakse infot, palju meil igat looma farmis mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();
    //Siin lisame farmi loomad(mapping, key - value, listi maxAnimalCounts.)
    // Siin hoitakse infot, kes keda sööb.
    private Map<String, String> whoEatsWho = new HashMap<String, String>();

    public Farm() {
        maxAnimalCounts.put("Pig", 2);
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 3);
    }

    public void addAnimal (Animal animal) {
        // Kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        if (!FarmAnimal.class.isInstance(animal)){
            System.out.println("Farmis saavad elada ainult farmi loomad");
            return;
        }
        // Vaatame, kas selline loom sobib farmi
        String animalType = animal.getClass().getSimpleName();
        if (!maxAnimalCounts.containsKey(animalType))
        {
            System.out.println("Farmis üldse sellistele loomadele kohta pole");
            return;
        }

        //Teame palju sigu mahub
        int maxAnimalCount = maxAnimalCounts.get(animalType);
        int animalCount = 0;
        if (animalCounts.containsKey(animalType)){
            animalCount = animalCounts.get(animalType);
            if (animalCount >= maxAnimalCount) {
                System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                return;
            }
            animals.add((FarmAnimal)animal);
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole farmis
            // kindlasti sellele loomale kohta on
        } else {
            animalCounts.put(animalType, 1);
        }
        animals.add((FarmAnimal)animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);
    }

    //Tee meetod, mis prindib välja kõik farmis elavad loomad, ning mitu neid on.
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
    //Tee meetod, mis eemaldab farmist looma.
    public void removeAnimal(String animalType) {
    // Kuidas leida nimekirjast loom, kelle nimi on Kalle
        //Teen muutuja, mis tahan, et iga element listist oleks : list mida tahan läbi käia
        //FarmAnimal animal hakkab olema järjest esimene loom,
        //siis teine loom, kolmas ja seni kuni loomi on.
        boolean animalFound = false;
        for (FarmAnimal animal : animals) {
            if (animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Farmist eemaldati loom %s%n", animalType);
                // kui see oli viimane loom, siis eemalda see rida animalCounts map-ist.
                // muuljuhul vähenda animalCounts map-is seda kogust.
                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }
        }
        if (!animalFound) {
            System.out.println("Farmis antud loom puudub");

        }


    }
}

