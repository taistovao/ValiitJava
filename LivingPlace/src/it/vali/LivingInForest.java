package it.vali;

public interface LivingInForest {

    void animalFoodChain (String whoEatsWho);
    void addAnimal (Animal animal);
    void printAnimalCounts();
    void removeAnimal(String animalType);

}
