package it.vali;

public class Main {

    public static void main(String[] args) {

        LivingPlace livingplace = new Farm();
        LivingInForest livinginforest = new Forest();

        Pig pig = new Pig();
        pig.setName("Kalle");
        livingplace.addAnimal(new Cat());
        livingplace.addAnimal(new Horse());
        livingplace.addAnimal(new Pig());
        livingplace.addAnimal(new Pig());
        livingplace.addAnimal(new Pig());

        Cow cow = new Cow();
        cow.setName("Priit");
        livingplace.addAnimal(new Cow());

        livingplace.removeAnimal("Pig");
        livingplace.removeAnimal("Pig");
        livingplace.printAnimalCounts();

        livinginforest.addAnimal(new Wolf());
        livinginforest.printAnimalCounts();
        livinginforest.removeAnimal("Wolf");
        livinginforest.animalFoodChain("Lynx");
        livinginforest.addAnimal(new Cat());

        // Mõelge ja täiendage zoo ja forest klasse nii, et neil oleks nende
        // 3 meetodi sisud


    }
}
