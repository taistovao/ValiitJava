package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {
    // Loome animal listi
    private List<zooAnimal> animals = new ArrayList<zooAnimal>();

    // Siin hoitakse infot, palju meil igat looma loomaaias on.
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma loomaaeda mahub.
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    //Siin lisame loomaaeda loomad(mapping, key - value, listi maxAnimalCounts.)
    public Zoo() {
        maxAnimalCounts.put("Elephant", 4);
        maxAnimalCounts.put("Giraffe", 2);
        maxAnimalCounts.put("Gorilla", 5);
        maxAnimalCounts.put("Buffalo", 9);
        maxAnimalCounts.put("Cheetah", 3);
        maxAnimalCounts.put("Monkey", 15);
        maxAnimalCounts.put("Panda", 7);
        maxAnimalCounts.put("Zebra", 8);
        maxAnimalCounts.put("Hippo", 10);
    }

    public void addAnimal (Animal animal) {
        // Kas animal on tüübist zooAnimal või pärineb sellest tüübist
        if (!zooAnimal.class.isInstance(animal)){
            System.out.println("Loomaaias saavad elada ainult loomaaia loomad");
            return;
        }
        // Vaatame, kas selline loom sobib loomaaeda
        String animalType = animal.getClass().getSimpleName();
        if (!maxAnimalCounts.containsKey(animalType))
        {
            System.out.println("Loomaaias sellistele loomadele kohta pole");
            return;
        }

        //Teame palju sigu mahub
        int maxAnimalCount = maxAnimalCounts.get(animalType);
        int animalCount = 0;
        if (animalCounts.containsKey(animalType)){
            animalCount = animalCounts.get(animalType);
            if (animalCount >= maxAnimalCount) {
                System.out.println("Loomaaias on sellele loomale kõik kohad juba täis");
                return;
            }
            animals.add((zooAnimal) animal);
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole loomaaias
            // kindlasti sellele loomale kohta on
        } else {
            animalCounts.put(animalType, 1);
        }
        animals.add((zooAnimal) animal);
        System.out.printf("Loomaaeda lisati loom %s%n", animalType);
    }
    //Tee meetod, mis prindib välja kõik farmis elavad loomad, ning mitu neid on.
    @Override
    public void printAnimalCounts() {

        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    //Tee meetod, mis eemaldab farmist looma.
    @Override
    public void removeAnimal(String animalType) {

        boolean animalFound = false;
        for (zooAnimal animal : animals) {
            if (animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Loomaaiast eemaldati loom %s%n", animalType);

                // kui see oli viimane loom, siis eemalda see rida animalCounts map-ist.
                // muuljuhul vähenda animalCounts map-is seda kogust.
                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }
        }
        if (!animalFound) {
            System.out.println("Loomaaias antud loom puudub");

        }

    }
}
