--See on lihtne hello world teksti p�ring, mis tagastab �he rea ja �he veeru
--(veerul puudub pealkiri). Selles veerus ja reas saab olema tekst Hello World.

SELECT'Hello World';


--T�isarvu saab k�sida ilma 
SELECT 3;

--Ei t��ta PostgreSQL,is. N�iteks MSSql'is t��tab.
SELECT 'toomas'+ 'k�is' + 'joomas';


--Standard CONCAT t��tab k�igis erinevates SQL serverites.
SELECT CONCAT ('toomas ', 'k�is ', 'joomas',' kell', ' 2' );


--Kui tahan erinevaid veerge, siis panen v��rtustele koma, vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'


--AS m�rks�naga saab anda antud veerule nime
SELECT 
	'Peeter' AS Eesnimi,
	'Paat' AS perekonnanimi,
	23 AS vanus,
	75.45 As pikkus,
	'Blond' AS juuksev�rv
	
--N�itab praegust kellaaega, kuup�eva, mingis vaikimisi formaadis.
SELECT NOW();
--Kui tahan konkreetset osa sellest, nt aastat v kuud, siis:
SELECT date_part('year',NOW())
SELECT 
	'Peeter' AS Eesnimi,
	'Paat' AS perekonnanimi,
	23 AS vanus,
	75.45 As pikkus,
	'Blond' AS juuksev�rv,
	NOW() AS kuup�ev;
--Kui tahan mingit kuup�eva isa ise etteantud kuup�evast
SELECT date_part ('month', TIME '10:10')	
	
--Kuup�eva formaatimine Eesti kuup�eva formaati.	
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY')	

-- Interval laseb lisada v�i eemaldada mingit aja�hikut
SELECT NOW () + interval '1 day ago' v�tab tagasi
SELECT NOW () + interval '1 day' l�heb edasi 1 p�ev
SELECT NOW () + interval '2 centuries 2 years 2 months 2 weeks'


CREATE TABLE 
id serial PRIMARY KEY, 
-- serial t�hendab, et t��biks on int, mis hakkab
-- �he v�rra suurenema. PRIMARY KEY (primaarv�ti) t�hendab, et see on 
-- unikaalne v�li tabelis
	first_name varchar (64) NOT NULL, --Ei tohi t�hi olla(NOT NULL)
	last_name varchar (64) NOT NULL,
	height int NULL,-- Tohib t�hi olla, ei pea sisestama
	weight numeric(5, 2) NULL,
	birthday date NULL


--See on tabelist k�ikide ridade k�ikide veergude k�simine.
--* siit t�hendab, et anna k�ik veerud.
SELECT * FROM student;
	
--Kui tahan otsida mingi tingimuse j�rgi, siis kasutame WHERE m�rks�na.	
	SELECT
	*
FROM 
	student
WHERE
	height = 100;
	
--K�si tabelist eesnime ja perekonnanime j�rgi mingi peeter ja mingi mari.
FROM 
	student
WHERE
	first_name = 'Peeter'
	AND last_name = 'Termomeeter'
	OR first_name = 'Kiisu'
	AND last_name = 'Miisu';

--Anna mulle �pilased kelle pikkus j��b 50-150cm vahele.
SELECT
	*
FROM 
	student
WHERE
	height >= 50 OR height <= 150;	
-- Anna mulle �pilased, kes on pikemad kui 170cm v�i l�hemad kui 150cm.	
SELECT
	*
FROM 
	student
WHERE
	height >= 30
	AND
	height <= 100;
	
--Anna mulle �pilaste eesnimed ja perekonnanimed, kelle s�nnip�ev on jaanuaris	
SELECT
	*
FROM 
	student
WHERE
	date_part('month',birthday)=1.
	
--Anna mulle �pilased, kelle middle_name on null (m��ramata).
SELECT
	*
FROM 
	student
WHERE
	middle_name IS NULL;	
--Anna mulle �pilased, kelle pikkus ei ole 180 cm.	Toimib ka <>, ! asemel.
SELECT
	*
FROM 
	student
WHERE
	height != 180	
--Anna mulle �pilased, kelle pikkus on 130,210,30.	
SELECT
	*
FROM 
	student
WHERE
	height = 130 OR height = 210 OR height = 30;
-- teine variant
SELECT
	*
FROM 
	student
WHERE
	height IN (130,210,30);
--Anna mulle �pilased, kelle eesnimi on Peeter, Kiisu v�i Andrus.	
SELECT
	*
FROM 
	student
WHERE
	first_name IN ('Peeter','Andrus','Kiisu');	
--Anna mulle �pilased, kelle s�nnikuup�ev on kuu 21.,14. p�ev.	
SELECT
	*
FROM 
	student
WHERE
	date_part ('day', birthday) IN (21,14);

--Anna mulle k�ik �pilased pikkuse j�rjekorras l�hemast pikemaks
SELECT
	*
FROM
	student
ORDER BY
	height
--Kui pikkused on v�rdsed, j�rjesta kaalu j�rgi.
SELECT
	*
FROM
	student
ORDER BY
	height, weight

--Tahan tagurpidises j�rjekorras, siis lisandub s�na DESC (descending)
--Olemas on ka ASC (Ascending), mis on vaikev��rtus.
SELECT
	*
FROM
	student
ORDER BY
	weight DESC

--Anna mulle vanuse j�rjekorras vanemast nooremaks, �pilaste pikkused, mis j��vad
-- 30-180 vahele.	
SELECT
	*
FROM
	student
WHERE
	height >= 30 AND height <=180
ORDER BY
	birthday;	
	
--Kui tahame leida t�he j�rgi tabelist nimesi, s�na alguses = K%, s�na keskel = %k%,
-- s�na l�pus %k.
SELECT
	*
FROM
	student
WHERE
	first_name LIKE 'K%';	
	
-- V�in otsida LIKE taha v�lja kitjutades ('Jaan') v�i alumised varjandid
SELECT
    *
FROM
    student
WHERE
    first_name LIKE 'Ka%' -- eesnimi algab 'Ka'
    OR first_name Like '%ee%' -- eesnimi sisalgab 'ee'
    OR first_name LIKE '%aan' -- eesnimi l�peb 'aan'
	
--Tabelisse kirjete lisamine	
INSERT INTO  student
	(first_name, last_name,height, weight, birthday, middle_name)
VALUES
	('Tiit','Tihane', 175, 87.55,'1984-02-08', 'Rebane'),
	('Teet','Desperado', 185, 457.55,'1923-03-11', NULL),
	('Taat','Maalt', 195, 87.55,'1987-07-06','Hobusega'),
	('Titt','Sigane', 133, 143,'1944-04-12', NULL)
	
--Tabelis kirje muutmine, N�ITES MUUDAB TERVE TABELI HEIGHT V��RTUSED 172
--alati peab kasutama WHERE lause l�pus.
UPDATE 
	student
SET
	height = 172		
--Info uuendamine	
UPDATE 
	student	
SET
	height = 199,
	weight = 500,
	middle_name = 'Johnny',
	birthday = '1300-05-03'
	
WHERE
	id = 20;	

--Muuda k�igi �pilaste pikkus �he v�rra suuremaks.
UPDATE 
	student	
SET
	height = height + 1

--Suurenda hiljem kui 1999 s�ndinud �pilastel s�nnip�eva �he p�eva v�rra.
	UPDATE 
	student		
SET
	birthday = birthday + interval '1 day'
WHERE
	date_part('year', birthday) > 1999;

--Kustutamisega olla ettevaatlik, alati kasuta WHERE'i.
DELETE FROM
	student
WHERE
	id = 4;
	
-- CRUD operations 
-- Create(insert), Read(select), Update, Delete.

--Loo uus tabel LOAN, millel on v�ljad: amount(reaalarv komaga), start_date,
-- due_date, student_id,	
	
CREATE TABLE LOAN (
id serial PRIMARY KEY,
	amount numeric(11,2) NOT NULL,
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL );

--Lisa nelja �pilase laenud.
--Kahele neist lisa veel 1 laen.
INSERT INTO LOAN
	(amount, start_date, due_date, student_id)
VALUES	
	(20000, '01.01.2002','01.03.2019',3),
	(30000, '01.02.2008','31.10.2020',4),
	(40000, '01.03.2005','30.09.2021',5),
	(50000, '01.04.2009','31.08.2022',6)
	
--Anna mulle k�ik �pilased koos oma laenudega
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id	
	
--Anna mulle k�ik �pilased koos oma laenudega
--aga ainult sellised laenud, mis on suuremad kui 500
--j�rjesta laenu suuruse j�rgi, suuremast v�iksemaks.
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
ON student.id = loan.student_id

WHERE
	loan.amount > 500
ORDER BY
	student.last_name,loan.amount DESC	
	 
INNER JOIN  --Selline tabelite liitmine, kus liidetakse ainult need read,
-- kus tabelite vahel on seos (student.id = loan.student_id),
--�lej��nud read ignoreeritakse.

--Loo uus tabel loan_type, milles v�ljad name ja description.
CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(500) NULL
);

-- Uue v�lja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int

--Tabeli kustutamine
DROP TABLE student;

--Lisame m�ned laenut��bid
INSERT INTO loan_type
	(name,description)
VALUES
	('�ppelaen', 'See on v�ga hea laen'),
	('SMSlaen', 'See on v�ga halb laen'),
	('Kodulaen', 'See on v�ga pikk laen'),
	('V�ikelaen', 'See on v�ga k�rge intressiga laen')
--Joinisime loan_type tabeli?	
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN 
	loan_type AS lt 
	ON lt.id = l.loan_type_id	
	
--Teistpidi p��rates on sama tulemus

SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan_type AS lt
JOIN
	loan AS l
	ON l.loan_type_id = lt.id
JOIN
	student AS s
	ON s.id = l.student_id	
	
-- LEFT JOIN puhul v�etakse joini esimesest (VASAKUST) tabelist k�ik read
-- ning teises tabelis (paremas) n�idatakse puuduvatel kohtadel NULL	
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN -- v�tab student tabelist k�ik nimed ja toob v�lja k�ik laenud(isegi kui kellegil pole laenu), kui oleks
-- right join siis v�tab k�ik laenud(isegi kui pole k�ikidel laenudel nimesi, kes v�tsid laenud)
	loan AS l
	ON s.id = l.student_id	

--LEFT JOIN puhul on j�rjekord v�ga oluline	
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id	

--Annab k�ik kombinatsioonid kahe tabeli vahel
SELECT 
	s.first_name, st.first_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE
	s.first_name != st.first_name	
	

-- FULL OUTER Join on sama mis LEFT JOIN + RIGHT JOIN
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN -- 
	loan AS l
	ON s.id = l.student_id

-- Anna mulle k�igi kasutajate perekonnanimed, kes v�tsid sms-laenu 
-- ja kelle laenu kogus �le 10000 euro, tulemused j�rjesta laenu v�tja vanuse j�rgi
-- v�iksemast suuremaks.

SELECT
	s.last_name
FROM
	student	AS s
JOIN
	loan as l
ON s.id = l.student_id	
JOIN 
	loan_type AS lt
ON lt.id = l.loan_type_id
WHERE
	lt.name = 'SMSlaen'
	AND l.amount > 30000
ORDER BY
	s.birthday 
	
--Aggregate functions
--Aggregaatfunktsiooni selectis v�lja kutsudes kaob v�imalus samas select lauses
-- k�sida mingit muud v�lja tabelist, sest agregaatfunktsiooni tulemus
--on alati ainult 1 number ja seda ei saa kuidagi n�idata koos v�ljadega
--mida v�ib olla mitu rida.	

--AVG , j�etakse v�lja kus height on NULL
SELECT
	AVG(height)
FROM
	student
	
--Palju on �ks �pilane keskmiselt laenu v�tnud
--Arvestatakse ka neid �pilasi, kes ei ole laenu v�tnud (amount on NULL)
--COALESCE teeb null(v��rtus puudub) numbriks 0.(v��rtuseks 0)
SELECT
	AVG(COALESCE(loan.amount,0))
	MIN(loan.amount)
FROM
	student
JOIN
	loan
ON student.id = loan.student_id	


--Palju on �ks �pilane keskmiselt laenu v�tnud
--aggregaatfunktsioonid
SELECT
	ROUND (AVG(COALESCE(loan.amount,0)),0) AS "Keskmine laenusumma",
	ROUND (MIN(loan.amount),0) AS "Miinimum laenusumma",
	ROUND (MAX(loan.amount),0) AS "Maksimum laenusumma",
	ROUND (SUM(loan.amount),0) AS "Laenude summa",
	COUNT (*) AS "K�ikide ridade arv",
	COUNT(loan.amount)AS "Laenude arv", --j�etakse v�lja read, kus loan.amount on NULL
	COUNT(student.height) AS "Mitmel �pilasel on pikkus"
FROM
	student
JOIN
	loan
ON student.id = loan.student_id	

-- Kasutades GROUP BY j��vad select p�ringu jaoks alles vaid need v�ljad, mis on
-- GROUP BY's �ra toodud (s.first_name, s.last_name)
-- Teisi v�lju saab ainult kasutada agregaatfunktsioonide sees
SELECT
	s.first_name, COUNT (s.last_name), SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_nam
	
--Anna mulle laenude summad laenut��pide j�rgi
SELECT
	 SUM (l.amount),(lt.name)
FROM
	loan as l
JOIN
	loan_type as lt
ON lt.id = l.loan_type_id
GROUP BY
	lt.name

--Anna mulle laenude summad s�nniaastate j�rgi.
SELECT
	 (date_part('year',birthday)), (SUM(COALESCE(l.amount,0)))
FROM
	loan as l
FULL OUTER JOIN
	student as s
ON l.student_id = s.id
GROUP BY
	date_part('year', s.birthday)
	
--HAVING on nagu WHERE aga peale group by kasutamist
-- filtreerimisel saab kasutada ainult neid v�lju, mis 
--on GROUP BY's ja agregaatfunktsioone
HAVING
	date_part('year', s.birthday)IS NOT NULL
	AND SUM(l.amount)>1000

--Tekita mingile �pilasele kaks sama t��pi laenu
--POOLIK!
SELECT
	 (date_part('year',birthday)), (SUM(COALESCE(l.amount,0)))
FROM
	loan as l
FULL OUTER JOIN
	student as s
ON l.student_id = s.id
GROUP BY
	date_part('year', s.birthday)
--HAVING on nagu WHERE aga peale group by kasutamist
-- filtreerimisel saab kasutada ainult neid v�lju, mis 
--on GROUP BY's ja agregaatfunktsioone
HAVING
	date_part('year', s.birthday)IS NOT NULL
	AND SUM(l.amount)>1000 -- anna ainult need read, kus summa oli suurem kui 1000
	AND COUNT (l.amount) = 2 -- anna ainult need read, kus laene kokku oli 2
ORDER BY
	date_part('year', s.birthday)
	
--Anna mulle laenude summad grupeerituna �pilase 
--ning laenu t��bi kaupa
--mari �ppelaen 2400 (1200+1200)
--mari v�ikelaen 2000
--mari �ppelaenu 2000
SELECT
	s.first_name, s.last_name, lt.name, SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	on l.student_id = s.id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	s.first_name, s.last_name, lt.name
	
--Anna mulle mitu laenu mingist t��bist on v�etud
--ning mis on nende summad.
--sms laenu 2tk 1000summa
--kodulaenu 3tk 40000 summa.

--Mis aastal s�ndinud v�tsid suurema summa rohkem laene?	



---Anna mulle �pilaste eesnime esit�he esinemise statistika
--ehk siis mitme �pilase eesnimi algab mingi t�hega.
--m 3
--a 2
--l 4

--mis s�nast, mitmendast t�hest ja mitu t�hte
--algab 1st

SELECT
	SUBSTRING(s.first_name,1 ,1), COUNT(SUBSTRING(s.first_name,1 ,1))	
FROM
	student AS s
GROUP BY
	SUBSTRING(s.first_name,1 ,1)
	
--Anna mulle �pilased, kelle pikkus vastab keskmisele �pilaste pikkusele
SELECT 
	first_name, last_name
FROM
	student
WHERE 
	height = (SELECT ROUND(AVG(height)) FROM student)


--Anna mulle �pilased, kelle eesnimi on keskmise pikkusega �pilaste keskmine nimi
SELECT
	first_name, last_name
FROM 
	student
WHERE
	first_name IN
(SELECT 
	middle_name
FROM
	student
WHERE 
	height = (SELECT ROUND(AVG(height)) FROM student))
	
	
--Lisa kaks vanimat �pilast t��tajate tabelisse
INSERT INTO employee(first_name, last_name, birthday, middle_name)
SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday LIMI

--Teisendamine t�isarvuks	
SELECT CAST(ROUND(AVG(age)) AS UNSIGNED) FROM emp99

	