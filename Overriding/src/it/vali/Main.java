package it.vali;

public class Main {

    public static void main(String[] args) {

        // Meetodi Overriding ehk meetodi ülekirjutamine
        // tähendab seda, et kuskil klassis, millest antud klass pärineb
        // oleva meetodi sisu kirjutatakse pärinevast klassist üle.
        // Päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle.

        Dog bulldog = new Dog();
        bulldog.eat();

        Cat meow = new Cat();
        meow.eat();
        meow.printInfo();

        // Kirjuta koera getAge üle nii, et kui koeral vanus on 0, siis näitaks
        // ikka 1.

        System.out.println(bulldog.getAge());
        System.out.println(meow.getAge());

        // Kirjuta koera getAge üle nii, et kui koeral vanus on 0, siis näiteks 1
        // Metsloomadel printinfo võiks kirjutada nii: metsloomadel pole nime
        meow.printInfo();

    }

}
