package it.vali;

public class Main {

    // Meetodid on mingid koodi osad, mis grupeerivad mingit teatud
    // kindlat funktsionaalsust.

    // kui koodis on korduvaid koodi osasid, võiks mõelda,
    // et äkki peaks nende kohta tegema eraldi meetodi.


    public static void main(String[] args) {

        printHello();
        printHello(3);
        printHello(2);
        printText("Kuidas läheb?");
        printText(2, "2");
    }

    // Lisame meetodi, mis prindib ekraanile Hello.

    private static void printHello() {

        System.out.println("Hello");

    }

    // Lisame meetodi, mis prindib Hello, etteanatud arv kordi

    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");
        }
    }   //Lisame meetodi, mis prindib ette antud teksti välja.

    //printText
    static void printText(String text) {

        System.out.println(text);

    } // Lisame meetodi, mis prindib ette antud teksti välja
    // ette antud arv kordi


    static void printText(int mitukorda, String text) {

        for (int i = 0; i < mitukorda; i++) {
            System.out.println(text);
        }

        }

        // Mis prindib ette antud teksti välja ette antud arv kordi.
        // lisaks saab öelda, kas tahame teha kõik tähed enne suureks või mitte
        static void printText ( int year, String text){

            System.out.printf("%d: %s%n,", year, text);

        }
        // Mis prindib ette antud teksti välja ette antud arv kordi.
        // lisaks saab öelda, kas tahame teha kõik tähed enne suureks või mitte
        static void printText(String text, int howManyTimes, boolean toUpperCase) {

            for (int i = 0; i < howManyTimes ; i++) {

            if (toUpperCase)

            } else {
                System.out.println(text);
            }
        }
         //POOLIK




}




        // Method OVERLOADING - meil on mitu meetodit sama nimega, aga erineva parameetrite
        // kombinatsiooniga.
        // Meetodi ülelaadimine








