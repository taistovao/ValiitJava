package it.vali;

public class Main {

    public static void main(String[] args) {

        // järjend, massiiv, nimikiri
        // Luuakse täisarvude massiiv, millesse mahub 5 elementi
        // Loomise hetkel määratud elementide arvu hiljem muuta ei saa.
        int[] numbers = new int[5];

        // Massiivi indeksid algavad 0st mitte 1st
        // viimane indeks on alati ühe võrra väiksem kui massiivi pikkus
        numbers[0] = 2;
        numbers[1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers[4] = 1;

//        System.out.println(numbers[3]);
//
//        for (int i = 0; i < numbers.length; i++) {
//
//            System.out.println(numbers[i]);
//
//        }
//        System.out.println("Uus ülesanne");
//        //Prindi numbrid tagurpidises järjekorras
//
//        for (int i = 4; i >= 0; i--) {
//            System.out.println(numbers[i]);
//        }
//        System.out.println("Uus ülesanne 2");
//        //Prindi numbrid, mis on suuremad kui 2
//        for (int i = 0; i < numbers.length; i++) {
//            if (numbers[i] > 2) {
//                System.out.println(numbers[i]);
//            }
//        }

//        System.out.println("Uus ülesanne 3");
//        //Prindi kõik paarisarvud
//
//        for (int i = 0; i < numbers.length; i++) {
//            //if (numbers[i] % 2 == 0) : tähendab et kui arvud on paaris
//            if (numbers[i] %2 == 0) {
//                System.out.println(numbers[i]);
//            }
//        }

        System.out.println("Uus ülesanne 3");
        //Prindi tagantpoolt 2 esimest paaritut arvu
        // if (numbers[i] % 2 !=0 : tähendab et kui arvud on paaritud.
        int counter = 0;
        for (int i = 4; i >= 0; i--) {
            if (numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if (counter == 2) {
                    break;
                }
            }
        }


        //Loo teine massiiv 3le numbrile ja pane sinna esimesest
        //massiivist 3 esimest numbrit.
        //Prindi teise massiivi elemendid ekraanile

        int[] secondNumber = new int[3];
        secondNumber [0] = numbers [4];
        secondNumber [1] = numbers [3];
        secondNumber [2] = numbers [2];

        for (int i = 0; i < numbers.length; i++) {
            secondNumber[i] = numbers[i];
        }
        for (int i = 0; i < secondNumber.length; i++) {
            secondNumber[i] = numbers[numbers.length - i - 1];
        }
            // Loo kolmas massiv 3le numbrile ja pane sinna esimesest massiivist
            //3 numbrit tagant poolt alates (1, 11, -2)
            for (int i = 0; i < secondNumber.length; i++) {
                System.out.println(secondNumber[i]);

            }

        System.out.println();

            int[] thirdNumbers = new int[3];
        for (int i = 0; i < numbers.length; i++) {
            thirdNumbers[i] = numbers[i];
        }
        for (int i = 0, j = numbers.length - 1; i < secondNumber.length ; i++, j--){

            secondNumber[i] = numbers[j];


        }

        }
    }

