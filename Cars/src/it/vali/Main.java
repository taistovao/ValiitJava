package it.vali;

public class Main {

    public static void main(String[] args) {
        Car bmw = new Car();
        bmw.startEngine();
        bmw.startEngine();
        bmw.stopEngine();
        bmw.stopEngine();
        bmw.accelerate(100);
        bmw.startEngine();
        bmw.accelerate(100);
        System.out.println("");

        Car opel = new Car("Opel", "Vectra", 1999, 205, 0);
        opel.startEngine();
        opel.accelerate(205);
        System.out.println("");

        Car audi = new Car("audi", "A8", 2010, 300, Fuel.PETROL, false);
        audi.startEngine();
        audi.accelerate(200);
        System.out.println(" ");

        Car volvo = new Car("volvo", "s80", 2007, 350, Fuel.DIESEL, true, 1, 100, "kmh");
        volvo.startEngine();
        volvo.accelerate(100);
        volvo.slowDown(40);
        System.out.println(" ");

        Car ferrari = new Car("Ferrari", "F140", 1990, 500, Fuel.PETROL, true, 1, 120, "kmh", true);
        ferrari.startEngine();
        ferrari.accelerate(140);
        ferrari.slowDown(30);
        ferrari.park(false);
        System.out.println("");

        Person person = new Person("");
        person.whoseCar("Lauri");


    }
}
