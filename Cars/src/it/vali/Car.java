package it.vali;

import java.util.ArrayList;
import java.util.List;

enum Fuel {GAS,DIESEL,PETROL, HYBRID, ELECTRIC}

public class Car {

    private String make;
    private String model, kmh, park;
    private int year;
    private Fuel fuel;
    private boolean isUsed, isParking;
    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed, minSpeed;
    private Owner owner;
    private Driver driver;
    private List<Person> passengers = new ArrayList<Person>();
    private int maxPassengers;


    public Car(String make, String model, Owner owner, Driver driver) {

        this.make = make;
        this.model = model;
        this.owner = owner;
        this.driver = driver;
    }
        public String getOwner(String owner) {

            return owner;
        }

        public void setOwner(Owner owner) {
            this.owner = owner;
        }

        public String getDriver(String driver) {
            return driver;
        }

        public void setDriver(Driver driver) {
            this.driver = driver;
        }


    public Car(String make, String model, int year, int maxSpeed, Fuel fuel, boolean isUsed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.fuel = fuel;
        this.isUsed = isUsed;
        this.passengers = passengers;
    }

    public Car(String make, String model, int year, int maxSpeed, Fuel fuel, boolean isUsed, int minSpeed, int speed, String kmh) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.fuel = fuel;
        this.isUsed = isUsed;
        this.minSpeed = minSpeed;
        this.speed = speed;
        this.kmh = kmh;
    }

    public Car(String make, String model, int year, int maxSpeed, Fuel fuel, boolean isUsed, int minSpeed, int speed, String kmh, boolean isParking) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.fuel = fuel;
        this.isUsed = isUsed;
        this.minSpeed = minSpeed;
        this.speed = speed;
        this.kmh = kmh;
        this.isParking = isParking;
    }

    // Konstruktor constructor
    // on eriline meetod, mis käivitatakse klassist objekti loomisel.
    public Car() {
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true;
        fuel = Fuel.PETROL;
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true;
        fuel = Fuel.PETROL;
    }

    public void Volvo() {
        System.out.println("Omanik: Taisto Vao");

    }

    // Constructor overloading
    public Car(String make, String model, int year, int maxSpeed, int minSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.minSpeed = minSpeed;


    }

//    public void Taisto(){
//
//    }

    public void startEngine() {
        if (!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        } else {
            System.out.println("Mootor juba töötab");
        }
    }

    public void stopEngine() {

        if (isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        } else {
            System.out.println("Mootor ei töötanud");
        }
    }

    public void accelerate(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        } else if (targetSpeed > maxSpeed) {
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimum kiirus on %d kmh %n", maxSpeed);
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada väiksemale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d kmh%n", targetSpeed);
        }
    }

    //slowDown (int targetSpeed) TEHTUD
    public void slowDown(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        } else if (targetSpeed < minSpeed) {
            System.out.println("Auto nii aeglaselt ei sõida");
            System.out.println("Auto miinimum kiirus on: " + minSpeed + kmh);
        } else if (targetSpeed < 1) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed > speed) {
            System.out.println("Auto ei saa aeglustuda suuremale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto aeglustus kuni kiiruseni %d kmh%n", targetSpeed);
        }
    }
    // park () mis tegevused oleks vaja teha (kutsu välja juba olemasolevaid meetodeid) TEHTUD


    public void park(boolean isParking) {
        if (isParking) {
            slowDown(1);
            System.out.println("Auto pargitakse tee äärde");
        } else {
            System.out.println("Auto ei leidnud parkimiseks kohta");
        }
    }
    // Lisa autole parameetrid driver ja owner (tüübist Person) ja nendele siis get ja set meetod EI TEA?!
    // Loo mõni auto objekt, kellel on siis määratud kasutaja ja omanik
    // Prindi välja auto omaniku vanus

    // lisa autole max reisijate arv
    // Lisa autole võimalus hoida reisijaid.
    // Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // ja kontrolli ka, et ei lisaks rohkem reisijaid kui mahub.

    public void addPassengers(Person passenger) {
        if (passenger.size() < maxPassengers) {
            passengers.add(passenger);
            System.out.printf("Autosse lisati reisija %s%n", passenger.getFirstName());
        } else {
            System.out.println("Autosse ei mahu enam reisijaid");
        }
    }
        public void removePassenger (Person passenger) {
            if (passengers.indexOf(passenger) != -1) {
            } else {
                System.out.println("Autos sellist reisijat pole");
            }

        }

        public void showPassengers () {
        // Foreach LOOP - iga elemendi kohta listis passengers tekita
            // objekt passenger
            //1. kordus Person passenger on esimene reisija
            //2. kordus Person passenger on teine reisija
            for (Person passenger : passengers)

                for (int i = 0; i < passenger.size() ; i++) {
                    System.out.println(passengers.get(i).getFirstName);
            }

        }


    }
