package it.vali;
//enum väärtused vastavad numbritele
enum Gender {FEMALE, MALE, NOTSPECIFIED}
enum Driver {TONY, JOSH, MIKE, SUZY, SANDRA}
enum Owner {KASS, KOER, KITS, KUKK, HAMSTER}
public class Person {

    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;
    private Driver driver;
    private Owner owner;

    public Person (String firstName){

        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }

    public void whoseCar (String firstName) {
        if (firstName.equals("Taisto")) {
            System.out.println("Volvo s80");
        } else if (firstName.equals("Lauri")){
            System.out.println("Tesla Model-S");
        } else if (firstName.equals("Egert")) {
            System.out.println("Hummer H1");
        } else {
            System.out.println("Puudub sellise nimega kasutaja!");
        }
    }


    public void carOwner (String firstName) {

    }

    // Kui klassil ei ole defineeritud konstruktorit,
    // siis tegelikult tehakse nähtamatu parameetrita konstruktor,
    // mille sisu on tühi.
    // Kui klassile ise lisada mingi konstruktor, siis see nähtamatu
    // parameetrita konstruktor kustutatakse
    // Sellest klassist saab siis teha objekti ainult selle uue konstruktoriga
}


