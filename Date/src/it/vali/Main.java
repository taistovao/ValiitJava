package it.vali;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        // Loome kalendri ja näitame ette kalendri formaadi
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss EEEEE");
        Calendar calendar = Calendar.getInstance();

        // move calendar to yesterday
        calendar.add(Calendar.DATE, -1);

        //Get current date of calendar which point to the yesterday now.
        Date date = calendar.getTime();
        System.out.println(dateFormat.format(date));

        calendar.add(calendar.YEAR, 1);
        date = calendar.getTime();
//        System.out.println(dateFormat.format(date));

        //Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäevad.

//        System.out.println(calendar.get(Calendar.MONTH));

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);

        date = calendar.getTime();

        DateFormat weekDayFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss EEEE");

//        int monthsLeftThisYear = 11 - calendar.get(Calendar.MONTH);
//        for (int i = 0; i < 11 - calendar.get(Calendar.MONTH); i++) {
//            calendar.add(Calendar.MONTH,1);
//            date = calendar.getTime();
//            System.out.println(dateFormat.format(date));


        int currentYear = calendar.get(Calendar.YEAR);
        calendar.add(Calendar.MONTH,1);

        while (calendar.get(Calendar.YEAR) == currentYear) {

            date = calendar.getTime();
            System.out.println(weekDayFormat.format(date));
            calendar.add(Calendar.MONTH,1);
        }
    }
}

