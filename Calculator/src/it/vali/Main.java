package it.vali;

public class Main {

    public static void main(String[] args) {

        int sum = sum(4, 5);
        System.out.printf("Arvude 4 ja 5 summa on %d%n", sum);
        System.out.printf("Arvude -12 ja -6 vahe on %d%n", subtract(-12, -6));
        System.out.printf("Arvude -12 ja -6 korruts on %d%n", multiply(-12, -6));

        int[] numbers = new int[3];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = -2;

        sum = sum(numbers);
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        numbers = new int[] { 2, 5, 12, -12};
        sum = sum(new int[] {2, 3, 12, 323, 43, 434, -11});
        System.out.printf("Massiivi arvude summa on %d%n", sum);
        sum = sum(new int[] {2, 3});

        int[] reversed = reverseNumbers(numbers);

        String [] sonad = new String [] {"Kala","Lammas","Koer","Kass","Lehm"};
        System.out.println(tyhik(sonad));
        System.out.println(tyhik(sonad,"_"));

        System.out.println(" ülemine");
        printNumbers(reversed);


        printNumbers(new int[] {1, 2, 3, 4, 5});

        printNumbers(reverseNumbers(new int[] {1, 2, 3, 4, 5}));

        printNumbers(convertToIntArray(new String[] { "2", "-12", "1", "0", "17" }));

        String[] numbersAsText = new String[] { "200", "-12", "1", "0", "17" };
        int[] numbersAsInt = convertToIntArray(numbersAsText);
        printNumbers(numbersAsInt);
        System.out.println("");
        int[] intArray = new int[]{ 1,2,3,4,5,6,7,8,9,10 };
        System.out.println(average(intArray));
    }

    // Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    // subract
    static int subtract(int a, int b) {
        return a - b;
    }

    // multiply
    static int multiply(int a, int b) {
        return a * b;
    }

    // divide
    static double divide(int a, int b) {
        return (double)a / b;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemndid kokku ning tagastab summa
    static int sum(int[] numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    // Meetod, mis võtab parameetriks täisarvude massivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1
    static int[] reverseNumbers(int[] numbers) {
        int[] reversedNumbers = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            reversedNumbers[i] = numbers[numbers.length - i - 1];
        }

        return  reversedNumbers;
    }

    // Meetod, mis prindib välja täisarvude massiivi elemendid
    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }

    // Meetod, mis võtab parameetiks stringi masiivi (eeldusel, et tegeilult seal massiivis on
    // numbrid stringidena) ja teisendab numbrite massiiviks ning tagastab selle
    static int[] convertToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        return numbers;
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause,
    // kus iga stringi vahel on tühik

    static String tyhik (String[] numbersAsText) {

        String lause = String.join(" ", numbersAsText); {

        }
        return lause;
    }




    // Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter on sõnade eraldaja
    // Tagastada lause.
    // Vaata eelmiset ülesannet, lihtsalt tühiku asemel saad ise valida, mille pealt sõnu eraldab

    static String tyhik (String[] numbersAsText,String delimiter) {

        String lause = String.join(delimiter, numbersAsText); {

        }
        return lause;
    }




    // Meetod, mis leiab numbrite massiivist keskmise ning tagastab selle

    static double average (int [] mingidnumbrid) {

        double sum = 0;
        for (int i = 0; i <mingidnumbrid.length ; i++) {
            sum += mingidnumbrid[i];
        }
        return sum/mingidnumbrid.length;


        // kui tahame kasutada %märki printf sees, siis kasutame topelt % ehk %%.


    }

    // Meetod, mis arvutab mitu % moodustab esimene arv teisest.



    // Meetod, ringi ümbermõõdu raadiuse järgi




    // kui tahame kasutada %märki printf sees, siis kasutame topelt % ehk %%.


}
