package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        int a = 100;
        short b = (short)a;

        a = b;

        double c = a;
        a = (int)c;
        // iga klassi võib võtta kui looma
        // implicit casting
        Animal animal = new Cat();

        // Iga loom ei ole kass
        // explicit casting
        Cat cat = (Cat) animal;

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        Cat Cat = new Cat();

        animals.add(dog);
        animals.add(Cat);
        animals.add(new Pet());

        //Kutsu kõikide listis olevate loomade printInfo välja.

        Cow cow = new Cow();
        cow.printInfo();

        for (Animal animalInList: animals) {
            animalInList.printInfo();
            System.out.println();
        }


        Animal

    }
}
