package it.vali;

public class Cat extends Domestic{

    private String name;
    private String breed;
    private int age;
    private double weight;

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }

    private boolean hasFur = true;

    public void catchMouse() {
        System.out.println("Püüdsin hiire kinni");
    }

    @Override
    public void eat () {
        System.out.println("Söön hiiri!");
    }

    public void printInfo() {
        // Superiga saan otsida eelnevast klassist, kui seal pole, siis veel eelnevat klassi jne. Vaatab kogu ahela läbi kuni
        // leiab printf meetodi
        super.printInfo();
        System.out.printf("Karvade olemasolu: %s%n", hasFur);
    }
}

