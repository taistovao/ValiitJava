package it.vali;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Main {

    public static void main(String[] args) {


        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            // readLine loeb igakord välja kutsudes järgmise rea.

            String line = bufferedReader.readLine();
            System.out.println(line);
            //kui raedLine avastab, et järgmist rida tegelikult ei ole,
            //siis tagastab see meetod null.
            
           while(line != null) {
               line = bufferedReader.readLine();
               System.out.println(line);
           }

            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){

        }

    }
}
