package it.vali;

public class Main {

    public static void main(String[] args) {

        int a = 4;
        //Kui arv on 4, printi välja.
        //muuljuhul kui arv on negatiivne siis kontrolli kas arv on suurem, kui -10
        //prindi sellekohane tekst

        //muuljuhul kontrolli kas arv on suurem kui 20.
        //ja prindi sellekohane tekst.
        if(a == 4){
            System.out.println("arv on 4");
        } else if (a < 0){
            System.out.println("arv on negatiivne");
        }
        if (a > -10){
            System.out.println("arv on suurem kui -10");
        } else if (a > 20){
            System.out.println("arv on suurem kui 20.");
        }


    }
}
