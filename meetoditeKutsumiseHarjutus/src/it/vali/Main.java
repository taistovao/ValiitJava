package it.vali;

public class Main {

    public static void main(String[] args) {

        printNumbers(reverseNumbers(new int[]{1, 2, 3, 4, 5}));

    }

    // Meetod, mis võtab parameetriks täisarvude massivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1
    static int[] reverseNumbers(int[] numbers) {
        int[] reversedNumbers = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            reversedNumbers[i] = numbers[numbers.length - i - 1];
        }

        return reversedNumbers;
    }


    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

    }
}
