package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        boolean isMinor = false;
        boolean isGrownUp = false;

        System.out.println("Kui vana sa oled?");
        Scanner scanner = new Scanner(System.in);

        int age = Integer.parseInt(scanner.nextLine());

        if (age < 18) {
            isMinor = true;

        }
        if (isMinor) {

            System.out.println("Oled alaealine");

        } else {

            System.out.println("Oled täiesealine");
        }
        int number = 4;

        boolean numberIsGreaterThan3 = number > 3;

        System.out.println(numberIsGreaterThan3);

        if (number > 2 && number < 6 || number > 10 || number < 20 || number == 100) {

        }
        boolean a = number > 2;
        boolean b = number < 6;
        boolean c = number > 10;
        boolean d = number < 20;
        boolean e = number == 100;
        boolean f = a && b;

        if (a && b || c || d || e) {

            boolean hasEaten = false;

            // Küsi kasutajalt kas ta on söönud hommikusööki
            System.out.println("Kas sa hommikusööki sõid. Kirjuta jah/ei");
            String answer = scanner.nextLine();
            if (answer.equals("jah")) {
                hasEaten = true;
            }
            // Küsi kasutajalt kas ta on söönud lõunat

            System.out.println("Kas sa lõunat sõid. Kirjuta jah/ei");
            answer = scanner.nextLine();
            if (answer.equals("jah")) {
                hasEaten = true;
            }

            System.out.println("Kas sa õhtust sõid. Kirjuta jah/ei");
            answer = scanner.nextLine();
            if (answer.equals("jah")) {
                hasEaten = true;
            }

            if (hasEaten) {
                System.out.println("Oled söönud");
            } else {
                System.out.println("Sa ei ole söönud");
            }

        }
    }
}