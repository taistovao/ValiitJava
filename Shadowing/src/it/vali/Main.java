package it.vali;

public class Main {
        static int a = 3;
    public static void main(String[] args) {

        int b = 7;
        System.out.println(b+a);

        a = 6;
        increaseByA(10);
        System.out.println(b+a);
        System.out.println(increaseByA(10));
        //Shadowing, ehk siis sees pool defineeritud muutuja
        //varjutab väljaspool oleva muutuja.

        int a = 6;
        System.out.println(b+a);

        //Nii saan klassi muutuja väärtust muuta
        Main.a = 8;
        //Meetodis defineeritud a väärtuse muutmine
        a = 5;
        System.out.println(increaseByA(10));

        System.out.println();
        System.out.println(b + Main.a);
        System.out.println(b+a);

    }

    static int increaseByA(int b) {

        return b+=a;
    }
}
