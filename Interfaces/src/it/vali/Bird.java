package it.vali;

public class Bird implements Flyer{

    @Override
    public void fly() {
        jumpUp();
        System.out.println("Lind lendab");
    }

    private void jumpUp() {
        System.out.println("Lind hüppas õhku");
    }
}
