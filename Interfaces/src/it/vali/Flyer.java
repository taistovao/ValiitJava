package it.vali;
// Bird klass kasutab/implementeerib liidest Flyer
// Interface ehk liides sunnib seda kasutavat/implementeerivat klassi
//omama liideses kirja pandud meetodeid (sama tagastuse tüübiga ja sama
// parameetrite kombinatsiooniga.

// Iga klass, mis interface kasutab, määrab ise ära meetodi sisu
public interface Flyer {
    void fly();

}
