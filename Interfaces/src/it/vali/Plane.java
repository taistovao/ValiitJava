package it.vali;
// Plane pärineb klassist Vehicle ja kasutab/implementeerib liidest Flyer
    public class Plane extends Vehicle implements Flyer, Driver {
    @Override
    public void fly() {
        doChecklist();
        startEngine();
        System.out.println("Lennuk lendab");
    }

    private void doChecklist() {
        System.out.println("Täidetakse checklist");
    }

    private void startEngine () {
        System.out.println("Mootor käivitus");
    }

    @Override
    public void drive() {

    }

    @Override
    public void stopDriving() {

    }
}
