package it.vali;

public class Car extends Vehicle implements Driver {
    private int maxDistance;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    private String make;
    @Override
    public void drive() {

    }

    @Override
    public void stopDriving() {

    }

    @Override
    public String toString() {
        return make;
    }
}





