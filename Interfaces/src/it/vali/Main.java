package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Flyer bird = new Bird();
        Flyer plane = new Plane();

        bird.fly();
        System.out.println();
        plane.fly();

        List<Flyer> flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();
        flyers.add(boeing);
        System.out.println(boeing);
        flyers.add(pigeon);

        System.out.println();
        for(Flyer flyer: flyers ) {
            flyer.fly();
            System.out.println();
        }

        Car car = new Car();
        car.setMake("Audi");
        System.out.println(car);

        // Lisa liides Driver, klass Car. Mõtle, kas lennuk ja auto võiks mõlemad
        // kasutada Driver liidest?
        // Driver liides võiks sisaldada 3 meetodi kirjeldust:
        // int getMaxDistance()
        // void drive()
        // void stopDriving(int afterDistance)- saad määrata mitme m pärast peatub
        // Pane auto ja lennuk mõlemad kasutama seda liidest
        // Lisa mootorratas
    }
}
