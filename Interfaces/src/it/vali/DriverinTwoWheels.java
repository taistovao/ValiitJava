package it.vali;

public interface DriverinTwoWheels extends Driver {

    void driveInRearWheel ();
}
