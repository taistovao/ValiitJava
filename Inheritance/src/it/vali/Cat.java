package it.vali;

public class Cat extends Domestic{

    private String name;
    private String breed;
    private int age;
    private double weight;

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }

    private boolean hasFur = true;

    public void catchMouse() {
        System.out.println("Püüdsin hiire kinni");
    }

}
