package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        String sentence = "Väljas on ilus ilm, tuult ei puhu, vihma ei saja ja päike paistab";

        //Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
        // | sümbol tähendab regulaaravaldises või
        String[] words = sentence.split(" ");
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }
        System.out.println("");
        //String Join ühendab sõnade massiivi lauseks.
        //(delimiter)tühik tähistab kohta, kust kohast sõnad pannakse kokku, words tähendab massiivi.
        String newSentence = String.join(" ", words);
        System.out.println(newSentence);
        newSentence = String.join("-", words);
        newSentence = String.join(".", words);
        System.out.println(newSentence);
        newSentence = String.join("\t", words);
        System.out.println(newSentence);
        //Escaping
        System.out.println("Juku ütles: \"Mulle meeldib suvi\"");
        System.out.println("");

        //Küsi kasutajalt rida numbreid nii, et ta paneb need numbrid kirja ühele reale
        // eraldades tühikuga. Seejärel liidab kõik numbrid kokku ja prindib vastuse(summa).
        System.out.println("sisesta arvud omavahel mida tahad liita, eraldades tühikuga");
        Scanner scanner = new Scanner(System.in);
        String numbersText = scanner.nextLine();
        String[] numbers = numbersText.split(" ");
        int sum = 0;
        for (int i = 0; i <numbers.length ; i++) {
            sum = sum + Integer.parseInt(numbers[i]);
        }
        String joinedNumbers = String.join(" ",numbers);
            System.out.printf(" Arvude %s summa on %s",joinedNumbers, sum);
        }
        }


