package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[5];


        // Lisa massiivi 5 numbrit

        numbers[0] = -4;
        numbers[1] = 2;
        numbers[2] = -4;
        numbers[3] = 14;
        numbers[4] = 7;

        // Eemalda siit teine number
        numbers[1] = 0;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
            int x = numbers[0];
            numbers[0] = numbers[3];
            numbers[3] = -4;
        }
        // Tavalisse arraylisti võin lisada ükskõik mis tüüpi elemente
        // aga elemente küsides sealt pean ma teadma, mis tüüpi element
        // kus täpselt asub ning pean siis selleks tüübiks küsimisel ka
        // teisendama (casting)
        List list = new ArrayList();

        list.add("Tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random();
        list.add(money);
        list.add(random);

        int a = (int) list.get(1);
        System.out.println(a);
        String word = (String) list.get(0);

        String sentence = list.get(1) + "hommikust";
        System.out.println(sentence);
        System.out.println("");


        list.add(2, "head aega");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(2.344354);
        list.addAll(otherList);

        System.out.println(otherList);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        if (list.contains(money)) {
            System.out.println("money asub listis");
        }

        System.out.printf("money asub listis indeksiga %d%n", list.indexOf(money));
        System.out.printf("44 asub listis indeksiga %d%n", list.indexOf(44));

        list.re
    }
}
