package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {


//        int a = 0;
//        try {
//            String word = null;
//            int b = 4 / a;
//            word.length();
//        }
//
//        //kui on mitu catch plokki, siis otsib ta esimes ploki, mis oskab
//        // antud exceptioni kinni püüda.
//        catch (ArithmeticException e) {
//            if (e.getMessage().equals("/ by zero")) {
//                System.out.println("Nulli ei saa jagada");
//            } else {
//                System.out.println("Esines aritmeetiline viga");
//            }
//        }
//        catch(RuntimeException e) {
//            System.out.println("Esines reaalajas viga");
//        }
//        //Exception on klass, millest kõik erinevad Exceptioni tüübid
//        //pärinevad. Mis omakorda tähendab, et püüdes kinni
//        //selle üldise exceptioni, püüame me kinni kõik Exceptionid.
//        catch (Exception e) {
//            System.out.println("esines viga");
//        }
//
//        //Küsime kasutajalt numbri(kirjutab tähe) ja kui number ei ole korrektne, siis ütleme mingi veateate
//        Scanner scanner = new Scanner(System.in);
//        boolean incorrectNumber;
//        do {
//            incorrectNumber = false;
//            System.out.println("Sisesta number: ");
//                                                                //   VAJA ÜLE VAADATA!!!!!!! boolean
//            try {
//                int vastus = Integer.parseInt(scanner.nextLine());
//            } catch (NumberFormatException e) {
//                System.out.println("Number oli vigane");
//                incorrectNumber = true;
//            }
//        } while (incorrectNumber);

        // Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv.
        // näita veateadet.
//        int [] numbers = new int [] {1,2,3,4,5};
//        try {
//            numbers[5] = 6;
//        } catch (ArrayIndexOutOfBoundsException e) {
//            System.out.println("Indeksit, kuhu tahtsid väärtust määrata, ei eksisteeri");
//        }
//    }


        int [] numbrid = new int[] {123,123,5646,876867,546,456,45,6,456,54,675,6};

        try {
            numbrid[55] = 44;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("vale number");
        }


    }


    }

