package it.vali;
import java.io.*;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static String PIN, result, insertCARD, pin;
    static double balance;
    static int choose;


    public static void main(String[] args) {

        System.out.println("Sisesta kaart: ");
        insertCARD = scanner.nextLine();
        for (int run = 0; run < 3; run++) {

            System.out.println("Sisesta PIN kood: ");
            PIN = scanner.nextLine();
            result = checkID(PIN);
            if (!result.equals("ERROR")) {
                break;
            } else if (run == 2) {// you cannot try to log in anymore than 3
                // times
                System.out.println("Maksimum katsed ületatud");
                return; //return hüppab välja meetodist. Pin valideerimine Lauri GitHubis
            }

        }
        menu();


    }

    public static String checkID(String PIN) {
        String result = "ERROR";
        String a = "1234";

        if (PIN.equals("1234")) {
            result = "1234";
        } else
            System.out.println("");
        return result;
    }

    public static int menu() {
        System.out.println("Vali tehing: \n1.Kontojääk\n2.Sularaha sissemakse\n3.Sularaha väljamakse\n4.Logi välja");
        choose = scanner.nextInt();

        if (choose == 1) {
            System.out.println(loadBalance());
            menu();
            return 1;

        }
        if (choose == 2) {
            deposit();
            saveBalance((int) balance);
            menu();
            return 2;

        }
        if (choose == 3) {
            withDraw();
            menu();
            saveBalance((int) balance);
            return 3;

        }
        if (choose == 4) {
            System.out.println("Oled välja loginud");
            return 4;

        }
        if (choose <= 5) {// type in anything greater than 4 and you will get a
            // system error
            System.out.println("Süsteemi viga");
            menu();
            return 5;
        }
        if (choose >= 1) {
            System.out.println("Süsteemi viga");
            menu();
            return 6;
        }
        return choose;
    }

    public static void deposit() {
        Scanner input = new Scanner(System.in);
        System.out.println("Sisesta sularaha sissemakse kogus: ");
        double amount = input.nextDouble();
        System.out.println("Sinu sularaha sissemakse kogus: " + amount + "€");
        balance += amount;
        System.out.println("Sinu uus kontojääk: " + balance + "€");
    }


    static int loadBalance() {
        // Toimub balance välja lugemine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\IntelliJ\\ATM2\\balance.txt"); //balance loetakse balance.txt välja
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine()); //balance tehakse int
            bufferedReader.close(); //suletakse bufferedreader?
            fileReader.close();//suletakse filereader

        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }

    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt"); //teeme uue fileWriteri
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }
    }

    public static void withDraw() {
        Scanner input = new Scanner(System.in);
        System.out.println("Sisesta väljamakse kogus: ");
        double amount = input.nextDouble();
        System.out.println("Sinu väljamakse kogus: " + amount + " €");
        balance -= amount;
        System.out.println("Sinu uus kontojääk: " + balance + "€");
    }

    static void savePin(String pin) {
        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }
    }

    static String loadPin() {
        // Toimub pin välja lugemine pin.txt failist
        // Toimub shadowing

        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi laadimine ebaõnnestus");
        }
        return pin;
    }
}
