package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt pin koodi, kui see on õige, siis ütleme
        // palju õnne
        // Kui pin kood vale, siis küsime uuesti .
        // kokku küsime 3 korda.

        String realPin = "1234";

        for (int i = 0; i < 3; i++) {

            System.out.println("Sisesta pin kood: ");
            Scanner kutsu = new Scanner(System.in);
            String enterPin = kutsu.nextLine();
            int t = 0;
            if (enterPin.equals(realPin)) {
                System.out.println("Tore! Õige pin kood");

                //break hüppab tsüklist välja
                int retriesLeft = 2;
                do {
                    System.out.println("Sisesta pin kood: ");
                    retriesLeft--;
                } while (!kutsu.nextLine().equals(enterPin) && retriesLeft > 0);

                if () {
                    System.out.println("panid 3 korda valesti");
                } else {
                    System.out.println("Tore, õige pin kood");
                }


        }


            } //prindi välja 10 kuni 20 ja 40 kuni 60
        //continue jätab selle tsüklikorduse katki ja läheb järgmise korduse juurde

        for (int i = 10; i <=60; i++) {
            if (i > 20 && i < 60) {
            continue;
            System.out.println(i);

        }
        }

    }

